<?php

namespace Drupal\simple_oauth_authentication_extender\Controller;

use Drupal\simple_oauth\Controller\Oauth2Token;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Overrides Oauth2Token controller.
 */
class TokenController extends Oauth2Token {

  /**
   * {@inheritdoc}
   *
   * @see http://drupal8.ovh/en/tutoriels/13/create-a-user-account-programmatically-drupal-8
   */
  public function content(ServerRequestInterface $request) {
    // Invoque the plugin class.
    $type = \Drupal::service('plugin.manager.oauth_extender_plugin');
    // Get a list of plugins available.
    $plugin_definitions = $type->getDefinitions();
    // Call each plugin to get the magic.
    foreach ($plugin_definitions as $plugin_definition) {
      /** @var \Drupal\simple_oauth_authentication_extender\Plugin\OauthExtenderPluginInterface $plugin */
      $plugin = $type->createInstance($plugin_definition['id'], ['default_handler' => $this]);
      /** @var \Psr\Http\Message\ResponseInterface $response */
      $response = $plugin->handleRequest($request);
      if ($response && $response->getStatusCode() == 200) {
        return $response;
      }
    }
    // Try default login.
    return parent::token($request);
  }

}
