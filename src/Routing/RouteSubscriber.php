<?php

namespace Drupal\simple_oauth_authentication_extender\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('oauth2_token.token')) {
      $route->setDefaults([
        '_controller' => '\Drupal\simple_oauth_authentication_extender\Controller\TokenController::content',
      ]);
    }
  }

}
