<?php

namespace Drupal\simple_oauth_authentication_extender\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Oauth extender plugin item annotation object.
 *
 * @see \Drupal\simple_oauth_authentication_extender\Plugin\OauthExtenderPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class OauthExtenderPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
