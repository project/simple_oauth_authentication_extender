<?php

namespace Drupal\simple_oauth_authentication_extender\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Defines an interface for Oauth extender plugin plugins.
 */
interface OauthExtenderPluginInterface extends PluginInspectionInterface {

  /**
   * Handle a OAuth request.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   The request.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   NULL if can't authorize the request.
   */
  public function handleRequest(ServerRequestInterface $request);

  /**
   * Default simple_oauth wrapper.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   The request.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   NULL if can't authorize the request.
   */
  public function defaultHandler(ServerRequestInterface $request);

}
