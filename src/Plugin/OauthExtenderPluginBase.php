<?php

namespace Drupal\simple_oauth_authentication_extender\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Base class for Oauth extender plugin plugins.
 */
abstract class OauthExtenderPluginBase extends PluginBase implements OauthExtenderPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function handleRequest(ServerRequestInterface $request) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultHandler(ServerRequestInterface $request) {
    $this->configuration['default_handler']->token($request);
  }

}
