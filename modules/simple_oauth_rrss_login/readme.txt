SDOS Smart City RRSS Login

It allow register users with social networks accounts.

This module overrides the oauth2_token.token route to register the user if the
mail, in email body parameter, don't exist and use the password grant_type.

The account token must come in the password body parameter.
