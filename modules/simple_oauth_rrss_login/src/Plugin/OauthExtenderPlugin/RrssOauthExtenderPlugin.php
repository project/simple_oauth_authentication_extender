<?php

namespace Drupal\simple_oauth_rrss_login\Plugin\OauthExtenderPlugin;

use Drupal\simple_oauth_authentication_extender\Plugin\OauthExtenderPluginBase;
use Psr\Http\Message\ServerRequestInterface;
use Drupal\Core\Entity\EntityStorageException;
use League\OAuth2\Server\Exception\OAuthServerException;
use GuzzleHttp\Psr7\Response;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Allow registration and login with social networks.
 *
 * @OauthExtenderPlugin(
 *  id = "rrss_oauth_extender_plugin",
 *  label = @Translation("RRSS authentication with registration."),
 * )
 */
class RrssOauthExtenderPlugin extends OauthExtenderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function handleRequest(ServerRequestInterface $request) {
    // Extract the grant type from the request body.
    $body = $request->getParsedBody();
    $grant_type_id = !empty($body['grant_type']) ? $body['grant_type'] : 'implicit';
    // If grant type is password and we have a email.
    if (!empty($body['username'])) {
      if ($grant_type_id == 'password' && isset($body["email"])) {
        // Do a social network login/registration.
        // Load the user.
        $password = $body["password"];
        $username = $this->cleanUserName($body["username"]);
        $email = $body["email"];
        /** @var \Drupal\user\Entity\User $user */
        $user = user_load_by_mail($email);
        // If the email is not registered.
        if (!$user) {
          // Validate, and clean, user name.
          // The username must be unique and accept only a-Z,0-9, - _ @ .
          $userNameNotOk = TRUE;
          $newUserName = $username;
          $cnt = 1;
          do {
            $userByName = user_load_by_name($newUserName);
            // User name in use?
            if ($userByName !== FALSE) {
              // Yes, add counter to user name.
              $newUserName = $username . ' ' . $cnt;
              ++$cnt;
            }
            else {
              // No, use it.
              $userNameNotOk = FALSE;
            }
          } while ($userNameNotOk);
          $username = $newUserName;
          // Register the user.
          $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
          $user = User::create();

          // Mandatory settings.
          $user->setPassword($password);
          $user->enforceIsNew();
          $user->setEmail($email);
          $user->setUsername($username);

          // Optional settings.
          $user->set("langcode", $language);
          $user->set("preferred_langcode", $language);
          $user->set("preferred_admin_langcode", $language);
          $user->activate();

          // Save user.
          try {
            $user->save();
          }
          catch (EntityStorageException $ex) {
            // We can't register.
            watchdog_exception('simple_oauth_rrss_login', $ex);
            $exception = new OAuthServerException($ex->getMessage(), $ex->getCode(), 'register_error');
            $response = $exception->generateHttpResponse(new Response());
            return $response;
          }
        }
        // Fix username to allow login.
        $user = user_load_by_mail($email);
        // Replace request user name.
        $_POST["username"] = $user->getAccountName();
        $request = $request->withParsedBody($_POST);
        // Try to login with the default handler and catch its response.
        return $this->defaultHandler($request);
      }
    }
    return NULL;
  }

  /**
   * Clean a user name.
   *
   * @param string $name
   *   The user name.
   *
   * @return null|string|string[]
   *   The clean user name or NULL.
   */
  protected function cleanUserName($name) {
    // Delete not valid characters.
    $name = preg_replace('/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i', '', substr($name, 0, UserInterface::USERNAME_MAX_LENGTH - 4));
    // Delete double spaces.
    $name = trim(preg_replace('/\s+/', ' ', $name));
    // Return the clean name.
    return $name;
  }

}
